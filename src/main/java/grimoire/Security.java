package grimoire;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

public class Security {
    public static boolean checkUserLogin(String user, String password) {
        try {
            password = hashAndSaltPassword(password, stringToByte(GrimoireDatabase.getUserSalt(user)));
            return GrimoireDatabase.checkUser(user, password);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean registerUser(String username, String password) {
        try {
            byte[] salt = generateSalt();
            password = hashAndSaltPassword(password, salt);
            return GrimoireDatabase.insertUser(username, password, byteToString(salt));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteUser(String username) {
        return GrimoireDatabase.removeUser(username);
    }

    private static String hashAndSaltPassword(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        final int STRENGTH = 65536;
        final String method = "PBKDF2WithHmacSHA1";
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, STRENGTH, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance(method);
        return byteToString(factory.generateSecret(spec).getEncoded());
    }

    private static byte[] generateSalt() {
        final int SALT_SIZE = 16;
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[SALT_SIZE];
        random.nextBytes(salt);
        return salt;
    }

    private static String byteToString(byte[] salt) {
        return Base64.getEncoder().encodeToString(salt);
    }

    private static byte[] stringToByte(String salt) {
        return Base64.getDecoder().decode(salt);
    }
}
