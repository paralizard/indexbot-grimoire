package grimoire;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import grimoire.grimoirepojos.GrimoireServerResponse;
import grimoire.grimoirepojos.GrimoireWebCommand;
import grimoire.grimoirepojos.Song;
import org.java_websocket.WebSocket;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GrimoireMessageHandler {
    static Logger logger = Logger.getLogger(GrimoireMessageHandler.class.getName());
    enum ServerError {
        AUTH_ERROR(1, "Error authorizing user/password"),
        DATA_ERROR(2, "Error parsing data received");

        String message;
        int type;

        ServerError(int i, String s) {
            message = s;
            type = i;
        }
    }

    public static void handleMessage(WebSocket conn, String message) {
        GsonBuilder builder = new GsonBuilder().setPrettyPrinting();
        Gson gson = builder.create();
        GrimoireWebCommand command = gson.fromJson(message, GrimoireWebCommand.class);
        // do checking for auth key here
        if(!Security.checkUserLogin(command.getUsername(), command.getPassword())) {
            GrimoireServerResponse response = generateErrorResponse(ServerError.AUTH_ERROR);
            conn.send(gson.toJson(response));
            logger.info("Command attempted by invalid authkey");
            conn.close();
            return;
        }
        logger.info(String.format("Received command %s from IP %s using username %s", command.getCommand(), conn.getRemoteSocketAddress(), command.getUsername()));
        switch(command.getCommand()) {
            case "sync":
                syncDatabase(conn, command);
                sendAllSongs(conn);
                break;
        }
    }

    private static void syncDatabase(WebSocket conn, GrimoireWebCommand command) {
        logger.log(Level.INFO, String.format("Performing sync by user %s", command.getUsername()));
        Gson gson = new GsonBuilder().create();
        String jsonSongArray = command.getData();
        Type collectionType = new TypeToken<Collection<Song>>(){}.getType();
        Collection<Song> songs = gson.fromJson(jsonSongArray, collectionType);
    }

    private static void sendAllSongs(WebSocket conn) {
        Collection<Song> songs = GrimoireDatabase.getAllSongs();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(songs);
        GrimoireServerResponse response = generateSongsResponse(json);
        String responseText = gson.toJson(response);
        conn.send(responseText);
        conn.close();
    }

    private static GrimoireServerResponse generateSongsResponse(String json) {
        GrimoireServerResponse response = new GrimoireServerResponse();
        response.setData(json);
        response.setError(0);
        return response;
    }

    private static GrimoireServerResponse generateErrorResponse(ServerError errorType) {
        GrimoireServerResponse error = new GrimoireServerResponse();
        error.setData(errorType.message);
        error.setError(errorType.type);
        return error;
    }


}
