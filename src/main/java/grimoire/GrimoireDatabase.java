package grimoire;

import grimoire.grimoirepojos.Song;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GrimoireDatabase {
    private static Connection connection;
    private static final String DB_NAME = "index_grimoire.db";
    private static Logger logger = Logger.getLogger(GrimoireDatabase.class.getName());

    public static void connect() {
        if(connection != null) {
            return;
        }
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        connection = null;
        try {
            String url = String.format("jdbc:sqlite:%s", DB_NAME);
            connection = DriverManager.getConnection(url);
            checkAndCreateTables();
            logger.info("Connected to SQLite Db successfully.");
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void disconnect() throws SQLException {
        if(connection != null) {
            connection.close();
            logger.info("SQLite DB connection closed.");
        }
    }

    private static boolean checkTableExists(String tableName) {
        try {
            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet rs = dbm.getTables(null, null, tableName, null);
            rs.last();
            return rs.getRow() > 0;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return false;
    }

    private static void checkAndCreateTables() {
        if(connection == null) {
            return;
        }
        String songs = "CREATE TABLE IF NOT EXISTS songs (\n"
                + " url text PRIMARY KEY,\n"
                + " anime text\n"
                + ");";
        String users = "CREATE TABLE IF NOT EXISTS users (\n"
                + " username text PRIMARY KEY,\n"
                + " password text,\n"
                + " salt text\n"
                + ");";
        try {
            Statement stmt = connection.createStatement();
            stmt.execute(songs);
            stmt = connection.createStatement();
            stmt.execute(users);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    public static boolean insertUser(String username, String password, String salt) {
        if(connection == null) {
            return false;
        }
        //language=SQLite
        String insertsql = "INSERT INTO users(username, password, salt) VALUES (?, ?, ?);";
        try {
            PreparedStatement pstmt = connection.prepareStatement(insertsql);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            pstmt.setString(3, salt);
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return false;
    }

    public static boolean removeUser(String username) {
        if(connection == null) {
            return false;
        }
        String removesql = "DELETE FROM users WHERE username = ?";
        try {
            PreparedStatement pstmt = connection.prepareStatement(removesql);
            pstmt.setString(1, username);
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return false;
    }

    public static boolean checkUser(String username, String password) {
        if(connection == null) {
            return false;
        }
        String sql = "SELECT username, password FROM users WHERE username = ?";
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, username);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            return password.equals(rs.getString("password"));
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return false;
    }

    public static List<Song> getAllSongs() {
        List<Song> songs = new ArrayList<>();

        String sql = "SELECT url, anime FROM songs";
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                Song song = new Song();
                song.setAnime(rs.getString("anime"));
                song.setUrl(rs.getString("url"));
                songs.add(song);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return songs;
    }

    public static String getUserSalt(String username) {
        String sql = "SELECT salt FROM users WHERE username = ?";
        try {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, username);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            return rs.getString("salt");
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }
}
