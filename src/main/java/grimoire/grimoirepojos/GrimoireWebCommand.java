package grimoire.grimoirepojos;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class GrimoireWebCommand{

	@SerializedName("data")
	private String data;

	@SerializedName("username")
	private String username;

	@SerializedName("password")
	private String password;

	@SerializedName("command")
	private String command;

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setCommand(String command){
		this.command = command;
	}

	public String getCommand(){
		return command;
	}

	@Override
 	public String toString(){
		return 
			"GrimoireWebCommand{" + 
			"data = '" + data + '\'' + 
			",username = '" + username + '\'' +
					",password = '" + password + '\'' +
			",command = '" + command + '\'' + 
			"}";
		}
}