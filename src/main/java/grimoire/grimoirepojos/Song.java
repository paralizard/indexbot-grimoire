package grimoire.grimoirepojos;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Song{

	@SerializedName("url")
	private String url;

	@SerializedName("anime")
	private String anime;

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setAnime(String anime){
		this.anime = anime;
	}

	public String getAnime(){
		return anime;
	}

	@Override
 	public String toString(){
		return 
			"Song{" + 
			"url = '" + url + '\'' + 
			",anime = '" + anime + '\'' + 
			"}";
		}
}