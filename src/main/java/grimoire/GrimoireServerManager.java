package grimoire;

import picocli.CommandLine;

import java.io.*;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

@CommandLine.Command(description = "Handles the running of the grimoire server", name = "GrimoireServerManager", mixinStandardHelpOptions = true, version = "IndexGrimoire 1.0")
public class GrimoireServerManager implements Callable {
    @CommandLine.Option(names = {"-p", "--port"}, required = true, paramLabel = "PORT", description = "the port to listen on")
    int port;
    @CommandLine.Option(names = {"-i", "--ip"}, required = false, paramLabel = "ADDRESS", description = "the ip address to bind to")
    String ip;

    static Logger logger = Logger.getLogger(GrimoireServerManager.class.getName());

    GrimoireServer server;

    public GrimoireServer buildServer() {
        try {
            return new GrimoireServer(port);
        } catch (UnknownHostException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        CommandLine.call(new GrimoireServerManager(), args);

    }

    public void acceptCommands() throws IOException, InterruptedException, SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        boolean shutdown = false;
        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter sysout = new BufferedWriter((new OutputStreamWriter(System.out)));
        while(!shutdown) {
            String command = sysin.readLine();
            switch(command) {
                case "exit":
                    closeServer();
                    shutdown = true;
                    break;
                case "adduser":
                    sysout.write("Username: ");
                    sysout.flush();
                    String username = sysin.readLine();
                    sysout.write("Password: ");
                    sysout.flush();
                    String password = sysin.readLine();
                    if(Security.registerUser(username, password)) {
                        sysout.write("User added successfully\n");
                    }
                    else {
                        sysout.write("Failed to add user\n");
                    }
                    sysout.flush();
                    break;
                case "removeuser":
                    sysout.write("Username: ");
                    sysout.flush();
                    String user = sysin.readLine();
                    sysout.write("Are you sure? (Y/n): ");
                    sysout.flush();
                    char choice = sysin.readLine().charAt(0);
                    if(choice != 'Y') {
                        break;
                    }
                    if(Security.deleteUser(user)) {
                        sysout.write("User deleted successfully.\n");
                    }
                    else {
                        sysout.write("An error occured. Does that user exist?\n");
                    }
                    sysout.flush();
                    break;
                case "logincheck":
                    sysout.write("Username: ");
                    sysout.flush();
                    String usern = sysin.readLine();
                    sysout.write("Password: ");
                    sysout.flush();
                    String pass = sysin.readLine();
                    sysout.write(Security.checkUserLogin(usern, pass) ? "Success!\n" : "Failed\n");
                    sysout.flush();
                    break;
                default:
                    sysout.write(String.format("Command \"%s\" not found\n", command));
                    sysout.flush();
            }
        }
    }

    public void closeServer() throws InterruptedException, SQLException {
        logger.info("Server shutting down.");
        server.stop(1000);
        GrimoireDatabase.disconnect();
    }

    @Override
    public Object call() throws Exception {
        server = buildServer();
        if(server == null) {
            System.out.println("Server is null, something did not initialize right");
            System.exit(1);
        }
        server.start();
        GrimoireDatabase.connect();
        logger.info(String.format("Server started on port %d", port));
        acceptCommands();
        return null;
    }
}
